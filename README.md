# Wack A Mole Game / Code Demo

This is a simple game I built using plain JavaScript.

## HTML, CSS and JavaScript

All project files can be found in the `/src` folder.

To run the project just fire up any node package that can serve static HTML pages such as `browser-sync`, `serve` or `nodemon` inside the src folder and open up your local browser.

### Instructions

Click click the **Start** button and use your mouse as the mole pop up and our of their holes to score points. You have 20 seconds to see how many points you can get.

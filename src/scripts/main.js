/**
 * Title: Wack-A-Mole Game
 * Description: See how many Moles you can destroy in 20 seconds.
 * Author: Philip Isaacs (showcasefloyd@gmail.com)
 * License: MIT
 */

(function() {
  // Game variables

  // Time in Milliseconds
  const maxDisplayTime = 2200;
  const minDisplayTime = 1000;
  const gameLengthTime = 20000;

  const totalMoleHoles = document.querySelectorAll('.mole-hole');
  const statusBar = document.getElementsByClassName('game-status');
  const scoreBoard = document.getElementsByClassName('game-scoreboard');
  const gameTimerDisplay = document.querySelector('.game-timer-display');

  const messages = [
    '<p>Press start to begin</p>',
    '<p>You have 20 seconds. Begin!</p>',
    '<p>Paused</p>',
    '<p>Game Over, Try Again?</p>',
    '<p>Resume</p>'
  ];

  let currentHole;
  let score;
  let gameOver = false;
  let gameRunning = false;
  let pauseGameFlag = false;
  let clearGameTimer;
  let clearMoleTimer;
  let timeTracker;
  let runningMoles = [];

  // Setup game board
  init();

  /**
   * Generate random time to show Mole
   * @param {Number} minTime - Minimum amount of time in milliseconds
   * @param {Number} maxTime - Maximum amount of time in milliseconds
   * @returns {Number}
   */
  function randomTimer(minTime, maxTime) {
    return Math.round(Math.random() * (maxTime - minTime) + minTime);
  }

  /**
   * Generate random Mole / hole to show
   * @param {Number} totalHoles - Number of holes in game
   * @returns {Number} randomNumber
   */
  function randomHole(totalHoles) {
    let hole = Math.floor(Math.random() * totalHoles);
    // Checking for dupes
    if (hole == currentHole) {
      return randomHole(totalHoles);
    }
    currentHole = hole;
    return hole;
  }

  /**
   * @param {Number} message - Sets the Status bar message display
   */
  function setStatusMessage(message) {
    statusBar[0].innerHTML = messages[message];
  }

  /**
   * Garbage collection. Clean up any Moles that were started and left hanging
   */
  function cleanUpMoles() {
    runningMoles.forEach(m => {
      clearInterval(m.id);
      totalMoleHoles[m.hole].classList.remove('show-mole');
    });
  }

  /**
   * Reset the runningMoles Array, Start showing Moles again.
   */
  function resumeGame() {
    runningMoles = [];
    showMole();
  }

  /**
   * Display Mole to the player by assigning a class to the hole
   */
  function showMole() {
    let rHole = randomHole(totalMoleHoles.length);
    let rTime = randomTimer(minDisplayTime, maxDisplayTime);
    let hole = totalMoleHoles[rHole];

    hole.classList.add('show-mole');

    clearMoleTimer = setTimeout(() => {
      hole.classList.remove('show-mole');
      if (gameOver) {
        setStatusMessage(3);
        gameRunning = false;
        return;
      }
      showMole();
    }, rTime);

    runningMoles.push({ id: clearMoleTimer, hole: rHole });
  }

  /**
   * Check to see if the player hit a hole with a mouse click
   * @param {event} e - Hit event
   */
  function moleHit(e) {
    score++;
    scoreBoard[0].innerHTML = score;
    e.target.classList.remove('show-mole');
  }

  /**
   * Pause game time and display "Pause" message
   */
  function pauseGame() {
    if (gameRunning) {
      // Pause / Unpause
      pauseGameFlag = !pauseGameFlag;

      if (pauseGameFlag) {
        setStatusMessage(2);
        clearInterval(clearGameTimer);

        // Sets up where the game timer should begin again
        gameTimeLeft = gameLengthTime;
        gameTimeLeft -= new Date() - timeTracker;
        gameTimerDisplay.innerHTML =
          'Time remaining: ' + (gameTimeLeft / 1000).toFixed(1) + ' Secs';

        cleanUpMoles();
      } else {
        setStatusMessage(4);

        clearGameTimer = setTimeout(() => {
          gameOver = true;
        }, gameTimeLeft);

        resumeGame();
      }
    }
  }

  /**
   * Stop timer, end game, display "Game Over" message
   */
  function endGame() {
    if (gameRunning) {
      gameOver = true;
      gameRunning = false;
      setStatusMessage(3);
      totalMoleHoles.forEach(hole => hole.classList.remove('show-mole'));
      gameTimerDisplay.innerHTML = 'Times up';
      clearInterval(clearGameTimer);
    }
  }

  /**
   * Start Game and Timer. Display "Start" message
   * Note: The game ends when timer runs out
   */
  function startGame() {
    // Don't start another game while one is running;
    if (!gameRunning) {
      gameOver = false;
      gameRunning = true;
      score = 0;
      secondsCountDown = 0;
      // Get the start of the game in Milliseconds
      timeTracker = new Date();

      scoreBoard[0].innerHTML = 0;
      gameTimerDisplay.innerHTML =
        'You have: ' + gameLengthTime / 1000 + ' Secs';
      setStatusMessage(1);

      clearGameTimer = setTimeout(() => {
        gameOver = true;
        setStatusMessage(3);
        gameRunning = false;
        gameTimerDisplay.innerHTML = 'Times up';
      }, gameLengthTime);

      showMole();
    }
  }

  /**
   * Setup before Game starts
   */
  function init() {
    setStatusMessage(0);
    // Grab DOM Elements
    document.getElementById('endGame').addEventListener('click', endGame);
    document.getElementById('startGame').addEventListener('click', startGame);
    document.getElementById('pauseGame').addEventListener('click', pauseGame);
    totalMoleHoles.forEach(hole => hole.addEventListener('click', moleHit));
  }
})();
